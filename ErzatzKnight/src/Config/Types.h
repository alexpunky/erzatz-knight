#ifndef TYPES_H
#define TYPES_H

struct Point {
    int x;
    int y;
};

enum Direction {
    UP, RIGHT, DOWN, LEFT
};


#endif;